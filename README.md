# Calculator Service

URL: http://infralabs.cs.ui.ac.id:20109/calculator (POST request)

Body should contain:
  - a (number)
  - b (number) (cannot be zero if op is div)
  - op (one of add, sub, mul, power, div)
