const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 
const port = 20109; 

app.post('/calculator', (req, res) => {
    const a = Number(req.body.a);
    const b = Number(req.body.b);
    const op = req.body.op;
    switch (op) {
        case "add":
            res.json({"result":a+b});
            break;
        case "sub":
            res.json({"result":a-b});
            break;
        case "mul":
            res.json({"result":a*b});
            break;
        case "power":
                res.json({"result":a**b});
                break;
        case "div":
            if (b == 0) { 
                res.json({"error":"b is zero!"})
                break;
            }
            res.json({"result":a/b});
            break;
    }
    
})

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))